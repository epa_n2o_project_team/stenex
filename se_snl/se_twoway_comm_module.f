! RCS file, release, date & time of last delta, author, state, [and locker]
! $Header: /tmp/CMAQv5.0.1/tarball/CMAQv5.0.1/models/STENEX/src/se_snl/se_twoway_comm_module.f,v 1.1.1.1 2012/04/19 19:48:15 sjr Exp $

! what(1) key, module and SID; SCCS file; date and time of last delta:
! %W% %P% %G% %U%

!-----------------------------------------------------------------------
! Purpose:
!   implement data exchange between WRF and CMAQ to accommodate different
!   domain sizes and domain decomposition strategies

! Revision history:
!   Orginal version: 4/10/07 by David Wong
!-----------------------------------------------------------------------

        module se_twoway_comm_module

        implicit none

        interface se_wrf_cmaq_comm
          module procedure se_wrf_cmaq_comm2, se_wrf_cmaq_comm3, se_wrf_cmaq_comm4
        end interface

        interface se_cmaq_wrf_comm
          module procedure se_cmaq_wrf_comm2, se_cmaq_wrf_comm3, se_cmaq_wrf_comm4
        end interface

        contains

! -----------------------------------------------------------------------------

        subroutine se_wrf_cmaq_comm2 (mype, wrf_data, cmaq_data, 
     &                                wrf_cmaq_send_to, wrf_cmaq_recv_from,
     &                                wrf_cmaq_send_index_l, wrf_cmaq_recv_index_l, 
     &                                flag)

        implicit none

        integer, intent(in) :: mype
        real, intent(in)  :: wrf_data(:,:)
        real, intent(out) :: cmaq_data(:,:)
        integer, pointer :: wrf_cmaq_send_to(:,:), wrf_cmaq_recv_from(:,:)
        integer, pointer :: wrf_cmaq_send_index_l(:,:,:), wrf_cmaq_recv_index_l(:,:,:)
        integer, intent(in) :: flag

        include "mpif.h"

        integer :: c, r, lc, lr, i, j, index, sdir, rdir, request, status(MPI_STATUS_SIZE), error
        integer :: data_size, tag, li, lj
        real :: sarray(size(wrf_data),9)  ! the second dimension is needed to keep the send
                                          ! data since all the send instructions are done
                                          ! before any receive instruction
        real :: rarray(size(cmaq_data))
        logical :: found

        do i = 1, wrf_cmaq_send_to(0, mype)

           sdir = wrf_cmaq_send_to(i, mype)

           j = (i - 1) * 3 + 1

           if (mype .eq. sdir) then

              found = .false.
              li = 0
              do while (.not. found)
                 li = li + 1
                 if (mype .eq. wrf_cmaq_recv_from(li, mype)) then
                    found = .true.
                    lj = (li - 1) * 3 + 1
                 end if
              end do

              lr = wrf_cmaq_recv_index_l(lj,2,mype) - 1
              do r = wrf_cmaq_send_index_l(j,2,mype), wrf_cmaq_send_index_l(j+1,2,mype)
                 lr = lr + 1
                 lc = wrf_cmaq_recv_index_l(lj,1,mype) - 1
                 do c = wrf_cmaq_send_index_l(j,1,mype), wrf_cmaq_send_index_l(j+1,1,mype)
                    lc = lc + 1
                    cmaq_data(lc,lr) = wrf_data(c,r)
                 end do
              end do

           else

              data_size = wrf_cmaq_send_index_l(j+2,1,mype) * wrf_cmaq_send_index_l(j+2,2,mype)

              index = 0
              do r = wrf_cmaq_send_index_l(j,2,mype), wrf_cmaq_send_index_l(j+1,2,mype)
                 do c = wrf_cmaq_send_index_l(j,1,mype), wrf_cmaq_send_index_l(j+1,1,mype)
                    index = index + 1
                    sarray(index,i) = wrf_data(c,r)
                 end do
              end do
              tag = flag * 1000000 + mype * 1000 + sdir

              call mpi_issend (sarray(1:data_size,i), data_size, mpi_real, sdir,
     &                         tag, mpi_comm_world, request, error)

!             call mpi_request_free (request, error)

           end if
        end do

        do i = 1, wrf_cmaq_recv_from(0, mype)

           rdir = wrf_cmaq_recv_from(i, mype)

           if (mype .ne. rdir) then
              j = (i - 1) * 3 + 1

              data_size = wrf_cmaq_recv_index_l(j+2,1,mype) * wrf_cmaq_recv_index_l(j+2,2,mype)

              tag = flag * 1000000 + rdir * 1000 + mype

              call mpi_recv (rarray(1:data_size), data_size, mpi_real, rdir, tag, mpi_comm_world, status, error)

              index = 0
              do r = wrf_cmaq_recv_index_l(j,2,mype), wrf_cmaq_recv_index_l(j+1,2,mype)
                 do c = wrf_cmaq_recv_index_l(j,1,mype), wrf_cmaq_recv_index_l(j+1,1,mype)
                    index = index + 1
                    cmaq_data(c,r) = rarray(index)
                 end do
               end do
            end if
        end do

        return
        end subroutine se_wrf_cmaq_comm2

! -----------------------------------------------------------------------------

        subroutine se_wrf_cmaq_comm3 (mype, wrf_data, cmaq_data, 
     &                                wrf_cmaq_send_to, wrf_cmaq_recv_from,
     &                                wrf_cmaq_send_index_l, wrf_cmaq_recv_index_l, 
     &                                flag)

        implicit none

        integer, intent(in) :: mype
        real, intent(in)  :: wrf_data(:,:,:)
        real, intent(out) :: cmaq_data(:,:,:)
        integer, pointer :: wrf_cmaq_send_to(:,:), wrf_cmaq_recv_from(:,:)
        integer, pointer :: wrf_cmaq_send_index_l(:,:,:), wrf_cmaq_recv_index_l(:,:,:)
        integer, intent(in) :: flag

        include "mpif.h"

        integer :: c, r, lc, lr, d, i, j, index, sdir, rdir, request, status(MPI_STATUS_SIZE), error
        integer :: data_size, size_3d, tag, li, lj
        real :: sarray(size(wrf_data),9)  ! the second dimension is needed to keep the send
                                          ! data since all the send instructions are done
                                          ! before any receive instruction
        real :: rarray(size(cmaq_data))
        logical :: found

        size_3d = size(wrf_data, 3)

        do i = 1, wrf_cmaq_send_to(0, mype)

           sdir = wrf_cmaq_send_to(i, mype)

           j = (i - 1) * 3 + 1

           if (mype .eq. sdir) then

              found = .false.
              li = 0
              do while (.not. found)
                 li = li + 1
                 if (mype .eq. wrf_cmaq_recv_from(li, mype)) then
                    found = .true.
                    lj = (li - 1) * 3 + 1
                 end if
              end do

              do d = 1, size_3d
                 lr = wrf_cmaq_recv_index_l(lj,2,mype) - 1
                 do r = wrf_cmaq_send_index_l(j,2,mype), wrf_cmaq_send_index_l(j+1,2,mype)
                    lr = lr + 1
                    lc = wrf_cmaq_recv_index_l(lj,1,mype) - 1
                    do c = wrf_cmaq_send_index_l(j,1,mype), wrf_cmaq_send_index_l(j+1,1,mype)
                       lc = lc + 1
                       cmaq_data(lc,lr,d) = wrf_data(c,r,d)
                    end do
                 end do
              end do

           else
              data_size = size_3d * wrf_cmaq_send_index_l(j+2,1,mype) * wrf_cmaq_send_index_l(j+2,2,mype)

              index = 0
              do d = 1, size_3d
                 do r = wrf_cmaq_send_index_l(j,2,mype), wrf_cmaq_send_index_l(j+1,2,mype)
                    do c = wrf_cmaq_send_index_l(j,1,mype), wrf_cmaq_send_index_l(j+1,1,mype)
                       index = index + 1
                       sarray(index,i) = wrf_data(c,r,d)
                    end do
                 end do
              end do
              tag = flag * 1000000 + mype * 1000 + sdir

              call mpi_isend (sarray(1:data_size,i), data_size, mpi_real, sdir,
     &                        tag, mpi_comm_world, request, error)

!             call mpi_request_free (request, error)
           end if
        end do

        do i = 1, wrf_cmaq_recv_from(0, mype)

           rdir = wrf_cmaq_recv_from(i, mype)

           if (mype .ne. rdir) then

              j = (i - 1) * 3 + 1

              data_size = size_3d * wrf_cmaq_recv_index_l(j+2,1,mype) * wrf_cmaq_recv_index_l(j+2,2,mype)

              tag = flag * 1000000 + rdir * 1000 + mype

              call mpi_recv (rarray(1:data_size), data_size, mpi_real, rdir, tag, mpi_comm_world, status, error)

              index = 0
              do d = 1, size_3d
                 do r = wrf_cmaq_recv_index_l(j,2,mype), wrf_cmaq_recv_index_l(j+1,2,mype)
                    do c = wrf_cmaq_recv_index_l(j,1,mype), wrf_cmaq_recv_index_l(j+1,1,mype)
                       index = index + 1
                       cmaq_data(c,r,d) = rarray(index)
                    end do
                  end do
              end do
           end if
        end do

        return
        end subroutine se_wrf_cmaq_comm3

! -----------------------------------------------------------------------------

        subroutine se_wrf_cmaq_comm4 (mype, wrf_data, cmaq_data, 
     &                                wrf_cmaq_send_to, wrf_cmaq_recv_from,
     &                                wrf_cmaq_send_index_l, wrf_cmaq_recv_index_l, 
     &                                flag)

        implicit none

        integer, intent(in) :: mype
        real, intent(in)  :: wrf_data(:,:,:,:)
        real, intent(out) :: cmaq_data(:,:,:,:)
        integer, pointer :: wrf_cmaq_send_to(:,:), wrf_cmaq_recv_from(:,:)
        integer, pointer :: wrf_cmaq_send_index_l(:,:,:), wrf_cmaq_recv_index_l(:,:,:)
        integer, intent(in) :: flag

        include "mpif.h"

        integer :: sdir, rdir, size_l_v, nlays, nvars, si, ri
        logical :: done

        nlays = size(wrf_data, 3)
        nvars = size(wrf_data, 4)
        size_l_v = nlays * nvars

        done = .false.
        si = 1
        ri = 1
        do while (.not. done)
           if ((si .le. wrf_cmaq_send_to(0, mype)) .and.
     &         (ri .le. wrf_cmaq_recv_from(0, mype))) then
              sdir = wrf_cmaq_send_to(si, mype)
              rdir = wrf_cmaq_recv_from(ri, mype)
              if ((sdir .eq. rdir) .and. (sdir <= mype)) then
                 call se_twoway_send4 (mype, si, sdir, wrf_data, cmaq_data,
     &                                 wrf_cmaq_recv_from, wrf_cmaq_send_index_l, 
     &                                 wrf_cmaq_recv_index_l, flag, size_l_v,
     &                                 nvars, nlays)
                 si = si + 1
                 if (sdir == mype) then
                    ri = ri + 1
                 end if
              else if (sdir .lt. rdir) then
                 call se_twoway_send4 (mype, si, sdir, wrf_data, cmaq_data,
     &                                 wrf_cmaq_recv_from, wrf_cmaq_send_index_l, 
     &                                 wrf_cmaq_recv_index_l, flag, size_l_v,
     &                                 nvars, nlays)
                 si = si + 1
              else if ((sdir .gt. rdir) .or.
     &                 ((sdir .eq. rdir) .and. (sdir > mype))) then
                 call  se_twoway_recv4 (mype, ri, rdir, cmaq_data,
     &                                  wrf_cmaq_recv_index_l, flag, size_l_v,
     &                                  nvars, nlays)
                 ri = ri + 1
              end if
           else if (si .le. wrf_cmaq_send_to(0, mype)) then
              sdir = wrf_cmaq_send_to(si, mype)
              call se_twoway_send4 (mype, si, sdir, wrf_data, cmaq_data,
     &                              wrf_cmaq_recv_from, wrf_cmaq_send_index_l, 
     &                              wrf_cmaq_recv_index_l, flag, size_l_v,
     &                              nvars, nlays)
              si = si + 1
           else if (ri .le. wrf_cmaq_recv_from(0, mype)) then
              rdir = wrf_cmaq_recv_from(ri, mype)
              call  se_twoway_recv4 (mype, ri, rdir, cmaq_data,
     &                               wrf_cmaq_recv_index_l, flag, size_l_v,
     &                               nvars, nlays)
              ri = ri + 1
           else
              done = .true.
           end if
        end do

        return
        end subroutine se_wrf_cmaq_comm4

! -----------------------------------------------------------------------------

        subroutine se_cmaq_wrf_comm2 (mype, cmaq_data, wrf_data, 
     &                                cmaq_wrf_send_to, cmaq_wrf_recv_from,
     &                                cmaq_wrf_send_index_l, cmaq_wrf_recv_index_l, 
     &                                flag)

        implicit none

        integer, intent(in) :: mype
        real, intent(in)  :: cmaq_data(:,:)
        real, intent(out) :: wrf_data(:,:)
        integer, pointer :: cmaq_wrf_send_to(:,:), cmaq_wrf_recv_from(:,:)
        integer, pointer :: cmaq_wrf_send_index_l(:,:,:), cmaq_wrf_recv_index_l(:,:,:)
        integer, intent(in) :: flag

        include "mpif.h"

        integer :: c, r, lc, lr, i, j, index, sdir, rdir, request, status(MPI_STATUS_SIZE), error
        integer :: data_size, tag, li, lj
        real :: sarray(size(cmaq_data),4)  ! the second dimension is needed to keep the send
                                           ! data since all the send instructions are done
                                           ! before any receive instruction
        real :: rarray(size(wrf_data))
        logical :: found

        do i = 1, cmaq_wrf_send_to(0, mype)

           sdir = cmaq_wrf_send_to(i, mype)

           j = (i - 1) * 3 + 1             ! 1st diminsion of *_index_l array has three components: 
                                           ! starting index, ending index, and distance
                                           ! between starting and ending indices

           if (mype .eq. sdir) then

              found = .false.
              li = 0
              do while (.not. found)
                 li = li + 1
                 if (mype .eq. cmaq_wrf_recv_from(li, mype)) then
                    found = .true.
                    lj = (li - 1) * 3 + 1
                 end if
              end do

              lr = cmaq_wrf_recv_index_l(lj,2,mype) - 1
              do r = cmaq_wrf_send_index_l(j,2,mype), cmaq_wrf_send_index_l(j+1,2,mype)
                 lr = lr + 1
                 lc = cmaq_wrf_recv_index_l(lj,1,mype) - 1
                 do c = cmaq_wrf_send_index_l(j,1,mype), cmaq_wrf_send_index_l(j+1,1,mype)
                    lc = lc + 1
                    wrf_data(lc,lr) = cmaq_data(c,r)
                 end do
              end do

           else

              data_size = cmaq_wrf_send_index_l(j+2,1,mype) * cmaq_wrf_send_index_l(j+2,2,mype)

              index = 0
              do r = cmaq_wrf_send_index_l(j,2,mype), cmaq_wrf_send_index_l(j+1,2,mype)
                 do c = cmaq_wrf_send_index_l(j,1,mype), cmaq_wrf_send_index_l(j+1,1,mype)
                    index = index + 1
                    sarray(index,i) = cmaq_data(c,r)
                 end do
              end do
              tag = flag * 1000000 + mype * 1000 + sdir

              call mpi_issend (sarray(1:data_size,i), data_size, mpi_real, sdir,
     &                         tag, mpi_comm_world, request, error)

!             call mpi_request_free (request, error)
           end if
        end do

        do i = 1, cmaq_wrf_recv_from(0, mype)

           rdir = cmaq_wrf_recv_from(i, mype)

           if (mype .ne. rdir) then

              j = (i - 1) * 3 + 1             ! 1st diminsion of *_index_l array has three components: 
                                              ! starting index, ending index, and distance
                                              ! between starting and ending indices

              data_size = cmaq_wrf_recv_index_l(j+2,1,mype) * cmaq_wrf_recv_index_l(j+2,2,mype)

              tag = flag * 1000000 + rdir * 1000 + mype

              call mpi_recv (rarray(1:data_size), data_size, mpi_real, rdir, tag, mpi_comm_world, status, error)

              index = 0
              do r = cmaq_wrf_recv_index_l(j,2,mype), cmaq_wrf_recv_index_l(j+1,2,mype)
                 do c = cmaq_wrf_recv_index_l(j,1,mype), cmaq_wrf_recv_index_l(j+1,1,mype)
                    index = index + 1
                    wrf_data(c,r) = rarray(index)
                 end do
              end do
           end if
        end do

        return
        end subroutine se_cmaq_wrf_comm2

! -----------------------------------------------------------------------------

        subroutine se_cmaq_wrf_comm3 (mype, cmaq_data, wrf_data, 
     &                                cmaq_wrf_send_to, cmaq_wrf_recv_from,
     &                                cmaq_wrf_send_index_l, cmaq_wrf_recv_index_l, 
     &                                flag)

        implicit none

        integer, intent(in) :: mype
        real, intent(in)  :: cmaq_data(:,:,:)
        real, intent(out) :: wrf_data(:,:,:)
        integer, pointer :: cmaq_wrf_send_to(:,:), cmaq_wrf_recv_from(:,:)
        integer, pointer :: cmaq_wrf_send_index_l(:,:,:), cmaq_wrf_recv_index_l(:,:,:)
        integer, intent(in) :: flag

        include "mpif.h"

        integer :: c, r, lc, lr, d, i, j, index, sdir, rdir, request, status(MPI_STATUS_SIZE), error
        integer :: data_size, size_3d, tag, li, lj
        real :: sarray(size(cmaq_data),4)  ! the second dimension is needed to keep the send
                                           ! data since all the send instructions are done
                                           ! before any receive instruction
        real :: rarray(size(wrf_data))
        logical :: found

        size_3d = size(cmaq_data, 3)

        do i = 1, cmaq_wrf_send_to(0, mype)

           sdir = cmaq_wrf_send_to(i, mype)

           j = (i - 1) * 3 + 1             ! 1st diminsion of *_index_l array has three components: 
                                           ! starting index, ending index, and distance
                                           ! between starting and ending indices
           if (mype .eq. sdir) then

              found = .false.
              li = 0
              do while (.not. found)
                 li = li + 1
                 if (mype .eq. cmaq_wrf_recv_from(li, mype)) then
                    found = .true.
                    lj = (li - 1) * 3 + 1
                 end if
              end do

              do d = 1, size_3d
                 lr = cmaq_wrf_recv_index_l(lj,2,mype) - 1
                 do r = cmaq_wrf_send_index_l(j,2,mype), cmaq_wrf_send_index_l(j+1,2,mype)
                    lr = lr + 1
                    lc = cmaq_wrf_recv_index_l(lj,1,mype) - 1
                    do c = cmaq_wrf_send_index_l(j,1,mype), cmaq_wrf_send_index_l(j+1,1,mype)
                       lc = lc + 1
                       wrf_data(lc,lr,d) = cmaq_data(c,r,d)
                    end do
                 end do
              end do

           else

              data_size = size_3d * cmaq_wrf_send_index_l(j+2,1,mype) * cmaq_wrf_send_index_l(j+2,2,mype)

              index = 0
              do d = 1, size_3d
                 do r = cmaq_wrf_send_index_l(j,2,mype), cmaq_wrf_send_index_l(j+1,2,mype)
                    do c = cmaq_wrf_send_index_l(j,1,mype), cmaq_wrf_send_index_l(j+1,1,mype)
                       index = index + 1
                       sarray(index,i) = cmaq_data(c,r,d)
                    end do
                 end do
              end do
              tag = flag * 1000000 + mype * 1000 + sdir

              call mpi_issend (sarray(1:data_size,i), data_size, mpi_real, sdir,
     &                         tag, mpi_comm_world, request, error)

!             call mpi_request_free (request, error)
           end if
        end do

        do i = 1, cmaq_wrf_recv_from(0, mype)

           rdir = cmaq_wrf_recv_from(i, mype)

           if (mype .ne. rdir) then

              j = (i - 1) * 3 + 1             ! 1st diminsion of *_index_l array has three components: 
                                              ! starting index, ending index, and distance
                                              ! between starting and ending indices

              data_size = size_3d * cmaq_wrf_recv_index_l(j+2,1,mype) * cmaq_wrf_recv_index_l(j+2,2,mype)

              tag = flag * 1000000 + rdir * 1000 + mype

              call mpi_recv (rarray(1:data_size), data_size, mpi_real, rdir, tag, mpi_comm_world, status, error)

              index = 0
              do d = 1, size_3d
                 do r = cmaq_wrf_recv_index_l(j,2,mype), cmaq_wrf_recv_index_l(j+1,2,mype)
                    do c = cmaq_wrf_recv_index_l(j,1,mype), cmaq_wrf_recv_index_l(j+1,1,mype)
                       index = index + 1
                       wrf_data(c,r,d) = rarray(index)
                    end do
                  end do
              end do
           end if
        end do

        return
        end subroutine se_cmaq_wrf_comm3

! -----------------------------------------------------------------------------

        subroutine se_cmaq_wrf_comm4 (mype, cmaq_data, wrf_data, 
     &                                cmaq_wrf_send_to, cmaq_wrf_recv_from,
     &                                cmaq_wrf_send_index_l, cmaq_wrf_recv_index_l, 
     &                                flag)

        implicit none

        integer, intent(in) :: mype
        real, intent(in)  :: cmaq_data(:,:,:,:)
        real, intent(out) :: wrf_data(:,:,:,:)
        integer, pointer :: cmaq_wrf_send_to(:,:), cmaq_wrf_recv_from(:,:)
        integer, pointer :: cmaq_wrf_send_index_l(:,:,:), cmaq_wrf_recv_index_l(:,:,:)
        integer, intent(in) :: flag

        integer :: sdir, rdir, size_l_v, nlays, nvars, si, ri
        logical :: done

        nlays = size(cmaq_data, 3)
        nvars = size(cmaq_data, 4)
        size_l_v = nlays * nvars

        done = .false.
        si = 1
        ri = 1
        do while (.not. done)
           if ((si .le. cmaq_wrf_send_to(0, mype)) .and.
     &         (ri .le. cmaq_wrf_recv_from(0, mype))) then
              sdir = cmaq_wrf_send_to(si, mype)
              rdir = cmaq_wrf_recv_from(ri, mype)
              if ((sdir .eq. rdir) .and. (sdir <= mype)) then
                 call se_twoway_send4 (mype, si, sdir, cmaq_data, wrf_data, 
     &                                 cmaq_wrf_recv_from, cmaq_wrf_send_index_l, 
     &                                 cmaq_wrf_recv_index_l, flag, size_l_v,
     &                                 nvars, nlays)
                 si = si + 1
                 if (sdir == mype) then
                    ri = ri + 1
                 end if
              else if (sdir .lt. rdir) then
                 call se_twoway_send4 (mype, si, sdir, cmaq_data, wrf_data, 
     &                                 cmaq_wrf_recv_from, cmaq_wrf_send_index_l, 
     &                                 cmaq_wrf_recv_index_l, flag, size_l_v,
     &                                 nvars, nlays)
                 si = si + 1
              else if ((sdir .gt. rdir) .or.
     &                 ((sdir .eq. rdir) .and. (sdir > mype))) then
                 call  se_twoway_recv4 (mype, ri, rdir, wrf_data, 
     &                                  cmaq_wrf_recv_index_l, flag, size_l_v,
     &                                  nvars, nlays)
                 ri = ri + 1
              end if
           else if (si .le. cmaq_wrf_send_to(0, mype)) then
              sdir = cmaq_wrf_send_to(si, mype)
              call se_twoway_send4 (mype, si, sdir, cmaq_data, wrf_data, 
     &                              cmaq_wrf_recv_from, cmaq_wrf_send_index_l, 
     &                              cmaq_wrf_recv_index_l, flag, size_l_v,
     &                              nvars, nlays)
              si = si + 1
           else if (ri .le. cmaq_wrf_recv_from(0, mype)) then
              rdir = cmaq_wrf_recv_from(ri, mype)
              call  se_twoway_recv4 (mype, ri, rdir, wrf_data, 
     &                               cmaq_wrf_recv_index_l, flag, size_l_v,
     &                               nvars, nlays)
              ri = ri + 1
           else
              done = .true.
           end if 
        end do

        return
        end subroutine se_cmaq_wrf_comm4

! -----------------------------------------------------------------------------

        subroutine se_twoway_send4 (mype, si, sdir, source_data, dest_data, 
     &                              recv_from, send_index_l, 
     &                              recv_index_l, flag, size_l_v,
     &                              nvars, nlays)

        integer, intent(in) :: mype, si, sdir, size_l_v, nvars, nlays
        real, intent(in)  :: source_data(:,:,:,:)
        real, intent(out) :: dest_data(:,:,:,:)
        integer, pointer :: recv_from(:,:)
        integer, pointer :: send_index_l(:,:,:), recv_index_l(:,:,:)
        integer, intent(in) :: flag

        include "mpif.h" 

        integer :: c, r, lc, lr, l, v, j, index, request, status(MPI_STATUS_SIZE), error
        integer :: data_size, tag, lj, li 
        real, allocatable :: sarray(:)
        logical :: found

        allocate (sarray(size(source_data)), stat=error)

           j = (si - 1) * 3 + 1            ! 1st diminsion of *_index_l array has three components: 
                                           ! starting index, ending index, and distance
                                           ! between starting and ending indices

           if (mype .eq. sdir) then

              found = .false.
              li = 0
              do while (.not. found)
                 li = li + 1
                 if (mype .eq. recv_from(li, mype)) then
                    found = .true.
                    lj = (li - 1) * 3 + 1
                 end if
              end do

              do v = 1, nvars
                 do l = 1, nlays
                    lr = recv_index_l(lj,2,mype) - 1
                    do r = send_index_l(j,2,mype), send_index_l(j+1,2,mype)
                       lr = lr + 1
                       lc = recv_index_l(lj,1,mype) - 1
                       do c = send_index_l(j,1,mype), send_index_l(j+1,1,mype)
                          lc = lc + 1
                          dest_data(lc,lr,l,v) = source_data(c,r,l,v)
                       end do
                    end do
                 end do
              end do

           else

              data_size = size_l_v * send_index_l(j+2,1,mype) * send_index_l(j+2,2,mype)

              index = 0
              do v = 1, nvars
                 do l = 1, nlays
                    do r = send_index_l(j,2,mype), send_index_l(j+1,2,mype)
                       do c = send_index_l(j,1,mype), send_index_l(j+1,1,mype)
                          index = index + 1
                          sarray(index) = source_data(c,r,l,v)
                       end do
                    end do
                 end do
              end do
              tag = flag * 1000000 + mype * 1000 + sdir

!             call mpi_issend (sarray(1:data_size), data_size, mpi_real, sdir,
!    &                         tag, mpi_comm_world, request, error)
              call mpi_send (sarray(1:data_size), data_size, mpi_real, sdir,
     &                         tag, mpi_comm_world, error)

!             call mpi_wait (request, status, error)
!             call mpi_request_free (request, error)
           end if

        deallocate (sarray)

        end subroutine se_twoway_send4

! -----------------------------------------------------------------------------

        subroutine se_twoway_recv4 (mype, ri, rdir, dest_data, 
     &                              recv_index_l, flag, size_l_v,
     &                              nvars, nlays)

        integer, intent(in) :: mype, ri, rdir, size_l_v, nvars, nlays
        real, intent(out) :: dest_data(:,:,:,:)
        integer, pointer :: recv_index_l(:,:,:)
        integer, intent(in) :: flag

        include "mpif.h"

        integer :: c, r, lc, lr, l, v, j, index, request, status(MPI_STATUS_SIZE), error
        integer :: data_size, tag
        real, allocatable :: rarray(:)
        logical :: found

        allocate (rarray(size(dest_data)), stat=error)

              j = (ri - 1) * 3 + 1            ! 1st diminsion of *_index_l array has three components: 
                                              ! starting index, ending index, and distance
                                              ! between starting and ending indices

              data_size = size_l_v * recv_index_l(j+2,1,mype) * recv_index_l(j+2,2,mype)

              tag = flag * 1000000 + rdir * 1000 + mype

              call mpi_recv (rarray(1:data_size), data_size, mpi_real, rdir, tag, mpi_comm_world, status, error)

              index = 0
              do v = 1, nvars
                 do l = 1, nlays
                    do r = recv_index_l(j,2,mype), recv_index_l(j+1,2,mype)
                       do c = recv_index_l(j,1,mype), recv_index_l(j+1,1,mype)
                          index = index + 1
                          dest_data(c,r,l,v) = rarray(index)
                       end do
                    end do
                  end do
              end do

        deallocate (rarray)

        end subroutine se_twoway_recv4 

      end module se_twoway_comm_module
