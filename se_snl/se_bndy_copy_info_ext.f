C RCS file, release, date & time of last delta, author, state, [and locker]
C $Header: /tmp/CMAQv5.0.1/tarball/CMAQv5.0.1/models/STENEX/src/se_snl/se_bndy_copy_info_ext.f,v 1.1.1.1 2012/04/19 19:48:15 sjr Exp $ 

C what(1) key, module and SID; SCCS file; date and time of last delta:
C %W% %P% %G% %U%

C --------------------------------------------------------------------------
C se_bngb_pe -- neighbouring PE in the boundary exchange communication 
C               pattern
C --------------------------------------------------------------------------

        module se_bndy_copy_info_ext

          integer :: se_bngb_pe(8)

        end module se_bndy_copy_info_ext
